using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public List<GameObject> throwButton;
    public GameObject needCardButton;
    public GameObject stopCardButton;
    public GameObject deckShuffleButton;
    
    public GameObject knob;
    public GameObject logoImage;
    public GameObject purgatoryText;
    public List<GameObject> menuButtonList;
    public List<GameObject> menuButtonTextList;

    public List<GameObject> selectNumAIButtons;
    public GameObject initialShuffleButton;

    public GameObject pauseMenuPanel;
    public GameObject howToPlayPanel;

    private void Update()
    {
        if(GameManager.Instance.startGame)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                CallPauseMenu();
            }
        }
    }

    public void SetActiveDealerButton(bool isActive)
    {
        needCardButton.SetActive(isActive);
        stopCardButton.SetActive(isActive);
    }

    public void StartGameButton()
    {
        logoImage.GetComponent<Animation>().Play("DissolveAnim");
        purgatoryText.GetComponent<Animation>().Play("DissolveAnim");
        
        foreach(GameObject button in menuButtonList)
        {
            button.GetComponent<Animation>().Play("DissolveAnim");
        }
        foreach (GameObject button in menuButtonTextList)
        {
            button.GetComponent<Animation>().Play("DissolveTextAnim");
        }

        GameManager.Instance.gamePhase = GamePhase.InitialSetup;
        GameManager.Instance.startGame = true;
        Cursor.lockState = CursorLockMode.Locked;
        knob.SetActive(true);

        deckShuffleButton.SetActive(true);
    }

    public void SetNumPlayer(int numPlayer)
    {
        GameManager.Instance.playerInGame = numPlayer;

        for (int i = 0; i < selectNumAIButtons.Count; i++)
        {
            if(i != numPlayer - 1)
            {
                selectNumAIButtons[i].GetComponent<Button>().interactable = true;
            }
            else
            {
                selectNumAIButtons[i].GetComponent<Button>().interactable = false;
            }
        }
    }

    public void SetInitialShuffle()
    {
        GameManager gm = GameManager.Instance;
        if (gm.initialShuffle)
        {
            gm.initialShuffle = false;
            initialShuffleButton.GetComponent<Image>().color = Color.white;
        }
        else
        {
            gm.initialShuffle = true;
            initialShuffleButton.GetComponent<Image>().color = Color.green;
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("GameplayScene");
    }

    public void CallPauseMenu()
    {
        GameManager gm = GameManager.Instance;
        if (gm.pauseGame)
        {
            knob.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;

            gm.pauseGame = false;
            pauseMenuPanel.SetActive(false);
            howToPlayPanel.SetActive(false);
        }
        else
        {
            knob.SetActive(false);
            Cursor.lockState = CursorLockMode.Confined;

            gm.pauseGame = true;
            pauseMenuPanel.SetActive(true);
        }
    }

    public void CloseGame()
    {
        Debug.LogError("CLOSE GAME");
        Application.Quit();
    }

}
