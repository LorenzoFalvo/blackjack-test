using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public enum GamePhase
{
    InitialSetup = 0,
    AITurn = 1,
    DealerCheck = 2,
    DealerTurn = 3,
    FinalCheck = 4,
    EndPhase = 5,

    SetupBoost = 6,
    InProgress = -1
}

public class GameManager : Singleton<GameManager>
{
    public GamePhase gamePhase;

    public DealerBehaviour dealerBh;
    public GameObject AIPrefab;
    public List<Brain> AIInGame;
    public List<Transform> spawnPoint;
    public int playerInGame = 1;
    public int pointsToWin = 21;
    public CardList deck;
    public List<CardInfo> currentDeck;
    public List<CardInfo> cardUsed;
    public List<int> playersPoints;
    public int dealerPoints;

    public bool initialShuffle;
    public bool startGame = false;
    public bool pauseGame = false;
    public bool playAI = false;
    public bool continueGameAI = false;
    public bool dealerTurn = false;
    public bool finalCheck = false;

    public TextMeshProUGUI dealerInfoText;

    

    protected void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    void Start()
    {
        gamePhase = GamePhase.InProgress;
    }

    // Update is called once per frame
    void Update()
    {
        switch (gamePhase)
        {
            case GamePhase.InitialSetup:

                dealerBh.canGrabDeck = false;
                StartGame();
                gamePhase = GamePhase.InProgress;
                break;

            case GamePhase.AITurn:

                if(hasAISplitted())
                {
                    //go to change setup
                    gamePhase = GamePhase.SetupBoost;
                    break;
                }

                if (hasAIReady())
                {
                    dealerBh.ShowDealerCards();
                    dealerInfoText.gameObject.SetActive(true);
                    gamePhase = GamePhase.DealerCheck;
                }
                
                break;

            case GamePhase.SetupBoost:

                CallSplitBoost();
                gamePhase = GamePhase.AITurn;
                break;

            case GamePhase.DealerCheck:

                DealerCheck();
                break;

            case GamePhase.DealerTurn:

                //Debug.LogError("DealerTurn Phase");
                if(!dealerCheckCard())
                {
                    gamePhase = GamePhase.FinalCheck;
                }
                break;

            case GamePhase.FinalCheck:

                //Debug.LogError("FinalPhase Phase");
                UIManager.Instance.SetActiveDealerButton(false);
                FinalCheck();
                break;

            case GamePhase.EndPhase:

                Debug.LogError("EndPhase Phase");

                EndPhase();
                gamePhase = GamePhase.InProgress;

                break;

            default:
                break;
        }

    }

    public void StartGame()
    {
        ResetAllInfo();

        //Set current deck
        foreach (CardInfo card in deck.cardInfo)
        {
            currentDeck.Add(card);
        }

        //Create and Set new player
        for (int i = 0; i < playerInGame; i++)
        {
            playersPoints.Add(0);
            GameObject currentAI = Instantiate(AIPrefab, spawnPoint[i].position, spawnPoint[i].rotation, spawnPoint[i]);
            currentAI.GetComponent<Brain>().maxValue = pointsToWin;
            currentAI.GetComponent<Brain>().tablePos = i;
            AIInGame.Add(currentAI.GetComponent<Brain>());
        }

        if(initialShuffle)
        {
            DeckShuffle();
        }

        StartCoroutine(SetInitialGame());
    }
    
    public void ResetAllInfo()
    {
        //Remove All Cards on Table
        foreach(CardComponent c in dealerBh.cardOnTable)
        {
            Destroy(c.gameObject);
        }
        foreach (CardComponent c in dealerBh.dealerCards)
        {
            Destroy(c.gameObject);
        }

        //Clear Game info
        currentDeck.Clear();
        playersPoints.Clear();
        AIInGame.Clear();

        //Clear Dealer info
        dealerInfoText.gameObject.SetActive(false);
        dealerPoints = 0;
        dealerBh.dealerCards.Clear();
        dealerBh.cardOnTable.Clear();
        dealerBh.nRound = 0;

        //Reset first and second card pos
        for(int i = 0; i < dealerBh.firstCardsPos.Count; i++)
        {
            dealerBh.firstCardsPos[i].position = dealerBh.startCardsPos[i];
        }
        for (int i = 0; i < dealerBh.secondCardsPos.Count; i++)
        {
            dealerBh.secondCardsPos[i].position = dealerBh.startSecondCardsPos[i];
        }
        dealerBh.dealerCardPos.position = dealerBh.startDealerCardPos;

    }

    public void DealerCheck()
    {
        if(dealerPoints == pointsToWin)
        {
            gamePhase = GamePhase.FinalCheck;
        }
        else if (!dealerCheckCard())    //Check dealer points 
        {
            dealerInfoText.text += ": Dealer Lost";
            gamePhase = GamePhase.FinalCheck;
        }
        else
        {
            dealerBh.dealerCardPos.gameObject.SetActive(true);
            UIManager.Instance.SetActiveDealerButton(true);
            gamePhase = GamePhase.DealerTurn;
        }
    }

    public void DealerPickCard()
    {
        StartCoroutine(dealerBh.SpawnDealerCards(currentDeck[0], true));
        SetDealerNewPoint();
    }

    
    public void DeckShuffle()
    {
        List<CardInfo> temporaryDeck = new List<CardInfo>();
        while (currentDeck.Count > 0)
        {
            int randomPick = Random.Range(0, currentDeck.Count);
            temporaryDeck.Add(currentDeck[randomPick]);
            currentDeck.RemoveAt(randomPick);
        }
        currentDeck = temporaryDeck;
    }

    IEnumerator SetInitialGame()
    {
        for(int i = 0; i < 2; i++)
        {
            for (int j = 0; j < playerInGame; j++)
            {
                //spawn first card of deck into player spawncards position
                StartCoroutine(dealerBh.SpawnCards(j, currentDeck[0]));
                yield return new WaitForSeconds(1);

                SetAINewPoint(j, true);
                //se � l'ultima carta del giro, quindi per il dealer
                if (j == playerInGame - 1)
                {
                    if (i==0)
                    {
                        StartCoroutine(dealerBh.SpawnDealerCards(currentDeck[0], false));
                        yield return new WaitForSeconds(1);
                        //carta del dealer coperta
                        SetDealerNewPoint();
                    }
                    else 
                    {
                        StartCoroutine(dealerBh.SpawnDealerCards(currentDeck[0], false));
                        yield return new WaitForSeconds(1);
                        //carta del dealer coperta
                        SetDealerNewPoint();
                    }
                }
            }

            dealerBh.nRound++;
        }

        for (int i = 0; i < AIInGame.Count; i++)
        {
            SetActiveCardPos(i, Action.Think, Action.Think);

            StartCoroutine(AIInGame[i].Thinking());
        }
        dealerBh.dealerCardPos.gameObject.SetActive(false);
        dealerBh.canGrabDeck = true;
        gamePhase = GamePhase.AITurn;
    }

    int PickFirstCard()
    {
        CardInfo cardPicked = currentDeck[0];
        cardUsed.Add(cardPicked);
        currentDeck.RemoveAt(0);
        return cardPicked.CardValue;
    }

    void PutCardsInDeck()
    {
        foreach(CardInfo c in cardUsed)
        {
            currentDeck.Add(c);
        }
        cardUsed.Clear();
    }

    public void GoToFinalCheck()
    {
        if (dealerBh.hasAce)
        {
            dealerPoints += 10;
            dealerInfoText.text = dealerPoints.ToString();
        }
        
        gamePhase = GamePhase.FinalCheck;
    }

    public void DropCardToAI(int playerPos)
    {
        StartCoroutine(dealerBh.SpawnCards(playerPos, currentDeck[0]));

        if (!AIInGame[playerPos].hasSplitted)
        {
            UpdateAIInfo(playerPos, true);
            dealerBh.firstCardsPos[playerPos].gameObject.SetActive(false);
        }
        else
        {
            CheckAIAfterDrop(playerPos);
        }
    }

    public void UpdateAIInfo(int playerPos, bool isFirstPos)
    {
        SetAINewPoint(playerPos, isFirstPos);
        CheckAIAfterDrop(playerPos);
    }

    public void CheckAIAfterDrop(int playerPos)
    {
        AIInGame[playerPos].myDecision = Action.Think;
        for (int i = 0; i < AIInGame.Count; i++)
        {
            if (AIInGame[i].myDecision == Action.Card || AIInGame[i].mySecondDecision == Action.Card) break;
            else
            {
                if (i == AIInGame.Count - 1)
                {
                    //Think AI 
                    foreach (Brain p in AIInGame)
                    {
                        StartCoroutine(p.Thinking());
                    }

                    dealerBh.nRound++;
                }
                else continue;
            }
        }
    }
    public void SetAINewPoint(int playerPos, bool isFirstPos)
    {
        int cardValue = PickFirstCard();
        Brain b = AIInGame[playerPos];

        //controllo se posso abilitare lo split al Brain
        if (dealerBh.nRound < 2 && cardValue == playersPoints[playerPos] && !b.hasSplitted)
        {
            b.canSplit = true;

            //controllo se la seconda carta � un asso
            if (cardValue == 1)
            {
                b.splitCardHasAce = false;
            }

            //entrambe le carte hanno lo stesso valore
            playersPoints[playerPos] = cardValue;
            b.myPoints = cardValue;
            b.mySecondPoints = cardValue;

            return;
        }

        //controllo quale punteggio va settato
        if (b.hasSplitted)
        {
            //se la prossima carta che vorr� � la seconda
            if(isFirstPos)
            {
                //calcolo carte prima posizione
                b.myPoints += CalculateCardValue(cardValue, b.myPoints, b);
            }
            else
            {
                //calcolo carte seconda posizione
                b.mySecondPoints += CalculateCardValue(cardValue, b.mySecondPoints, b);
            }
            
        }
        else
        {
            b.myPoints += CalculateCardValue(cardValue, b.myPoints, b);
        }

        playersPoints[playerPos] += cardValue;

        b.SetPoint();
    }

    public int CalculateCardValue(int value, int playerPoints, Brain b)
    {
        int cardValue = value;

        //se pesco l'asso
        if(cardValue == 1)
        {
            //if(dealerBh.nRound > 2)
            //{
            //    //l'asso vale 1
            //}
            //else
            {
                //se la AI ha gi� un asso
                if (b.hasAce)
                {
                    //l'asso vale 1
                }
                else
                {
                    //se valore maggiore > 21
                    if(cardValue + playerPoints + 10 > pointsToWin)
                    {
                        //l'asso vale 1
                    }
                    else if(cardValue + playerPoints + 10 == pointsToWin)
                    {
                        //l'asso vale 11
                        cardValue = 11;
                    }
                    else
                    {
                        //l'asso vale 1 o 11
                        //Scriviamo entrambi sul testo ma lasciamo il punteggio minore con la bool attiva
                        b.hasAce = true;
                    }
                }
            }
            
        }
        else
        {
            //se la AI ha gi� un asso
            if (b.hasAce)
            {
                if(cardValue + playerPoints + 10 > pointsToWin)
                {
                    //disattivo l'asso
                    b.hasAce = false;
                    //tengo il valore pi� basso dell'asso
                }
                else if(cardValue + playerPoints + 10 == pointsToWin)
                {
                    //disattivo l'asso
                    b.hasAce = false;
                    //prendo il valore pi� alto dell'asso
                    cardValue += 10;
                }
                else
                {
                    //Scriviamo entrambi sul testo ma lasciamo il punteggio minore con la bool attiva
                }
            }
        }

        return cardValue;
    }
    public void SetDealerNewPoint()
    {
        int cardValue = PickFirstCard();

        //se pesco l'asso
        if (cardValue == 1)
        {
            if (dealerBh.nRound > 2)
            {
                //l'asso vale 1
            }
            else
            {
                //se la AI ha gi� un asso
                if (dealerBh.hasAce)
                {
                    //l'asso vale 1
                }
                else
                {
                    //se valore maggiore > 21
                    if (cardValue + dealerPoints + 10 > pointsToWin)
                    {
                        //l'asso vale 1
                    }
                    else if (cardValue + dealerPoints + 10 == pointsToWin)
                    {
                        //l'asso vale 11
                        cardValue = 11;
                    }
                    else
                    {
                        //l'asso vale 1 o 11
                        //Scriviamo entrambi sul testo ma lasciamo il punteggio minore con la bool attiva
                        dealerBh.hasAce = true;
                    }
                }
            }

        }
        else
        {
            //se la AI ha gi� un asso
            if (dealerBh.hasAce)
            {
                if (cardValue + dealerPoints + 10 > pointsToWin)
                {
                    //disattivo l'asso
                    dealerBh.hasAce = false;
                    //tengo il valore pi� basso dell'asso
                }
                else if (cardValue + dealerPoints + 10 == pointsToWin)
                {
                    //disattivo l'asso
                    dealerBh.hasAce = false;
                    //prendo il valore pi� alto dell'asso
                    cardValue += 10;
                }
                else
                {
                    //Scriviamo entrambi sul testo ma lasciamo il punteggio minore con la bool attiva
                }
            }
        }

        dealerPoints += cardValue;

        if (dealerBh.hasAce)
        {
            int maxValue = dealerPoints + 10;
            dealerInfoText.text = dealerPoints.ToString() + "/" + maxValue.ToString();
        }
        else
        {
            dealerInfoText.text = dealerPoints.ToString();
        }
    }

    public void SetActiveCardPos(int playerPos, Action wantFirst, Action wantSecond)
    {
        if(wantFirst == Action.Card)
        {
            dealerBh.firstCardsPos[playerPos].gameObject.SetActive(true);
        }
        else
        {
            dealerBh.firstCardsPos[playerPos].gameObject.SetActive(false);
            if (wantSecond == Action.Card)
            {
                dealerBh.secondCardsPos[playerPos].gameObject.SetActive(true);
                AIInGame[playerPos].wantSecondCard = true;
            }
            else
            {
                dealerBh.secondCardsPos[playerPos].gameObject.SetActive(false);
            }
        }
    }

    #region Controll Functions
    public bool hasAIReady()
    {
        foreach(Brain b in AIInGame)
        {
            if (b.myDecision == Action.Think) return false;
            else if (b.myDecision == Action.Card) return false;
            else if (b.myDecision == Action.Boost) return false;
        }
        return true;
    }

    public bool hasAISplitted()
    {
        foreach (Brain b in AIInGame)
        {
            if (b.canSplit) return true;
        }
        return false;
    }

    public bool dealerCheckCard()
    {
        if (dealerPoints > pointsToWin)
        {
            return false;
        }
        return true;
    }
    #endregion

    public void FinalCheck()
    {
        if (dealerPoints == pointsToWin)
        {
            for (int i = 0; i < playerInGame; i++)
            {
                Brain AI = AIInGame[i];
                AI.myDecision = Action.Lost;
                AIInGame[i].myAction.text = "Lost!";
            }
        }
        else if (dealerPoints <= pointsToWin)
        {
            for (int i = 0; i < playerInGame; i++)
            {
                Brain AI = AIInGame[i];
                if (AI.myDecision == Action.Stop)
                {
                    if (AI.myPoints <= dealerPoints)
                    {
                        AI.myDecision = Action.Lost;
                        AIInGame[i].myAction.text = "Lost!";
                    }
                    else
                    {
                        AI.myDecision = Action.Won;
                        AIInGame[i].myAction.text = "Won!";
                    }
                }
                else if (AI.myDecision == Action.BlackJack)
                {
                    if (AI.myPoints > dealerPoints)
                    {
                        AI.myDecision = Action.Won;
                        AIInGame[i].myAction.text = "Won!";
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < playerInGame; i++)
            {
                Brain AI = AIInGame[i];
                if (AI.myDecision == Action.Stop || AI.myDecision == Action.BlackJack)
                {
                    AI.myDecision = Action.Won;
                    AIInGame[i].myAction.text = "Won!";
                }
            }
        }
        gamePhase = GamePhase.EndPhase;
    }

    public void EndPhase()
    {
        foreach (Brain b in AIInGame)
        {
            if (b.myDecision == Action.Won)
            {
                StartCoroutine(b.WinLoseAnim(1));
            }
            else if (b.myDecision == Action.Lost)
            {
                StartCoroutine(b.WinLoseAnim(-1));
            }
        }
    }

    public void CallSplitBoost()
    {
        //controllo quale AI devo far splittare
        foreach (Brain b in AIInGame)
        {
            if(b.hasSplitted)
            {
                b.canSplit = false;
                b.wantFirstCard = true;
                b.wantSecondCard = true;
                dealerBh.SetupSplitBoost(b);
            }
        }
    }
}
