using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public enum Action
{
    Card = 0,
    Stop = 1,
    Boost = 2,
    BlackJack = 4,

    Lost = -1,
    Won = 5,
    Think = 6,
}

public class Brain : MonoBehaviour
{
    public List<CardInfo> myCards;
    public int myPoints;
    public int maxValue;
    public int tablePos;

    public float maxThinkingTime;
    public TextMeshProUGUI myAction;
    public TextMeshProUGUI myPointsText;

    public Action myDecision;
    public Action mySecondDecision;

    public bool showInfo;
    public bool hasAce;
    public bool splitCardHasAce;

    Animator anim;

    public bool canSplit;
    public bool hasSplitted;
    public int mySecondPoints;
    public bool wantFirstCard, wantSecondCard;
    

    private void Start()
    {
        myDecision = Action.Think;
        mySecondDecision = Action.Think;

        anim = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if(showInfo && myDecision == Action.Card)
        {
            UIManager.Instance.throwButton[tablePos].SetActive(true);
        }
        else
        {
            UIManager.Instance.throwButton[tablePos].SetActive(false);
        }

    }
    public IEnumerator Thinking()
    {
        if (myDecision == Action.Think || myDecision == Action.Card)
        {
            myDecision = Action.Think;
            ModifyActionText("", "");
        }

        //if we want a random value of AI thinking time
        //float thinkingTime = Random.Range(0, maxThinkingTime);
        //yield return new WaitForSeconds(thinkingTime);

        yield return new WaitForSeconds(maxThinkingTime);

        myDecision = TakeDecisions();

        if(hasSplitted)
        {
            if(myDecision == mySecondDecision)
            {
                if (myDecision == Action.Card)
                {
                    wantFirstCard = true;
                    wantSecondCard = false;
                }

                DoAction(myDecision);
            }
            else if(myDecision == Action.Card || mySecondDecision == Action.Card)
            {
                if (myDecision == Action.Card) wantFirstCard = true;
                if (mySecondDecision == Action.Card) wantSecondCard = true;

                DoAction(Action.Card);
            }
            else if(myDecision == Action.Stop || mySecondDecision == Action.Stop)
            {
                DoAction(Action.Stop);
            }
            else if (myDecision == Action.BlackJack || mySecondDecision == Action.BlackJack)
            {
                DoAction(Action.BlackJack);
            }
            
        }
        else
        {
            DoAction(myDecision);
        }

        yield return null;
    }

    public void DoAction(Action action)
    {
        switch (action)
        {
            case Action.Card:
                
                GameManager.Instance.SetActiveCardPos(tablePos, myDecision, mySecondDecision);
                SetTriggerAnim("Card");
                break;

            case Action.Stop:
                
                SetTriggerAnim("Stop");
                break;

            case Action.Lost:

                SetTriggerAnim("Lost");
                break;

            case Action.BlackJack:
                SetTriggerAnim("Stop");
                break;

            case Action.Won:

                SetTriggerAnim("Won");
                break;

            case Action.Think:

                SetTriggerAnim("Think");
                break;

            case Action.Boost:

                hasSplitted = true;
                SetTriggerAnim("Boost");
                break;

            default:

                myAction.text = "Error!";
                break;
        }
    }

    Action TakeDecisions()
    {
        Debug.LogError("Make Decision");

        if(canSplit)
        {
            return Action.Boost;
        }

        myDecision = CheckCardsValue(ref myPoints);
        if(hasSplitted)
        {
            mySecondDecision = CheckCardsValue(ref mySecondPoints);
            ModifyActionText(myDecision.ToString(), mySecondDecision.ToString());
        }
        else
        {
            ModifyActionText(myDecision.ToString(), "");
        }

        Debug.LogError("FirstDecision: " + myDecision);
        Debug.LogError("SecondDecision: " + mySecondDecision);

        return myDecision;
    }

    Action CheckCardsValue(ref int cardsValue)
    {
        if (hasAce)
        {
            if (cardsValue + 10 >= 18)
            {
                hasAce = false;
                cardsValue += 10;
                SetPoint();
                return Action.Stop;
            }
            else if (cardsValue + 10 < 16)
            {
                return Action.Card;
            }
            else
            {
                int random = Random.Range(0, 10);
                if (random <= 5)
                {
                    return Action.Card;
                }
                else
                {
                    hasAce = false;
                    cardsValue += 10;
                    SetPoint();
                    return Action.Stop;
                }
            }
        }
        else
        {
            if (cardsValue > maxValue)
            {
                return Action.Lost;
            }
            else if (cardsValue == maxValue)
            {
                return Action.BlackJack;
            }
            else if (cardsValue < 16)
            {
                return Action.Card;
            }
            else
            {
                return Action.Stop;
            }
        }
    }

    public void SetPoint()
    {
        if (hasSplitted)
        {
            SetSplittedPoint();
        }
        else if(hasAce)
        {
            int maxValue = myPoints + 10;
            myPointsText.text = myPoints.ToString() + "/" + maxValue.ToString();
        }
        else
        {
            myPointsText.text = myPoints.ToString();
        }

    }

    public void SetSplittedPoint()
    {
        if(GameManager.Instance.dealerBh.nRound <= 2)
        {
            myPointsText.text = myPoints.ToString() + " - " + mySecondPoints.ToString();
        }
        else
        {
            if (hasAce)
            {
                int maxValue = myPoints + 10;
                string firstTextValue = myPoints.ToString() + "/" + maxValue.ToString();

                if (splitCardHasAce)
                {
                    int maxSecondValue = mySecondPoints + 10;
                    string secondTextValue = mySecondPoints.ToString() + "/" + maxSecondValue.ToString();

                    myPointsText.text = firstTextValue + " - " + secondTextValue;
                }
                else
                {
                    myPointsText.text = firstTextValue + " - " + mySecondPoints.ToString();
                }
            }
            else if(splitCardHasAce)
            {
                int maxSecondValue = myPoints + 10;
                string secondTextValue = mySecondPoints.ToString() + "/" + maxSecondValue.ToString();

                myPointsText.text = myPoints.ToString() + " - " + secondTextValue;
            }
            else
            {
                myPointsText.text = myPoints.ToString() + " - " + mySecondPoints.ToString();
            }
        }

    }

    public void ModifyActionText(string firstAction, string secondAction)
    {
        if(hasSplitted)
        {
            myAction.text = firstAction + " - " + secondAction;
        }
        else
        {
            myAction.text = firstAction;
        }
        
    }

    public IEnumerator WinLoseAnim(int direction)
    {
        if(direction > 0)
        {
            SetTriggerAnim("Won");
        }
        else
        {
            SetTriggerAnim("Lost");
        }

        float counter = 3f;
        float speedAnim = 4f;
        while(counter > 0)
        {
            transform.Translate((Vector3.up * direction) * Time.deltaTime * speedAnim);
            counter -= Time.deltaTime;
            yield return null;
        }

        if(GameManager.Instance.gamePhase != GamePhase.InitialSetup)
        {
            GameManager.Instance.gamePhase = GamePhase.InitialSetup;
        }
        Destroy(gameObject);
        yield return null;
    }

    public void SetTriggerAnim(string name)
    {
        anim.SetTrigger(name);
    }
}
