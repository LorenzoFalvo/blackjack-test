using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DealerBehaviour : MonoBehaviour
{
    public GameObject deckObj; 
    public GameObject cardPrefab;

    public List<Transform> firstCardsPos;
    public List<Transform> secondCardsPos;
    public List<Vector3> startCardsPos;
    public List<Vector3> startSecondCardsPos;
    public Transform dealerCardPos;
    public Vector3 startDealerCardPos;

    public Vector3 offsetCardsPos;
    public int nRound;

    public float speedThrowAnim;
    public float speedDeckAnim;

    public List<CardComponent> cardOnTable;
    public List<CardComponent> dealerCards;

    public Transform deckReleasedPos;
    public Transform deckTakedPos;
    public bool deckHasTaked;

    public float mouseScrollSpeed;
    public float speedMultiplier;
    public float timeToThrow;
    public float counter;
    public bool canThrow;

    public bool canGrabDeck;
    public bool hasAce;

    // Start is called before the first frame update
    void Awake()
    {
        //Copy firstcard and secondcard pos to use it
        foreach (Transform t in firstCardsPos)
        {
            Vector3 startPos;
            startPos = t.position;

            startCardsPos.Add(startPos);
        }
        foreach (Transform t in secondCardsPos)
        {
            Vector3 startPos;
            startPos = t.position;

            startSecondCardsPos.Add(startPos);
        }

        startDealerCardPos = dealerCardPos.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(canGrabDeck)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("Right Mouse Button has Pressed");
                deckHasTaked = true;
                StartCoroutine(TakeDeck());
            }
            if (Input.GetMouseButtonUp(1))
            {
                Debug.Log("Right Mouse Button has Pressed");
                deckHasTaked = false;
                StartCoroutine(ReleaseDeck());
            }

            if(Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                mouseScrollSpeed++;
                if(canThrow)
                {
                    canThrow = false;
                    StartCoroutine(StartCounter());
                }
            }
        }
        else
        {
            deckHasTaked = false;
            StartCoroutine(ReleaseDeck());
        }

    }

    #region Take/Release Deck LerpAnim
    public IEnumerator TakeDeck()
    {
        float lerpAnim = 0;
        Transform oldPos = deckObj.transform;
        canThrow = true;

        while (deckHasTaked)
        {
            Vector3 lerpPos = Vector3.Lerp(oldPos.position, deckTakedPos.position, lerpAnim);
            deckObj.transform.position = lerpPos;

            Quaternion lerpRot = Quaternion.Lerp(oldPos.rotation, deckTakedPos.rotation, lerpAnim);
            deckObj.transform.rotation = lerpRot;

            lerpAnim += Time.deltaTime * speedDeckAnim;
            yield return null;
        }

        yield return null;
    }

    public IEnumerator ReleaseDeck()
    {
        canThrow = false;

        float lerpAnim = 0;
        Transform oldPos = deckObj.transform;

        while (lerpAnim <= 1 && !deckHasTaked)
        {
            Vector3 lerpPos = Vector3.Lerp(oldPos.position, deckReleasedPos.position, lerpAnim);
            deckObj.transform.position = lerpPos;

            Quaternion lerpRot = Quaternion.Lerp(oldPos.rotation, deckReleasedPos.rotation, lerpAnim);
            deckObj.transform.rotation = lerpRot;

            lerpAnim += Time.deltaTime * speedDeckAnim;
            yield return null;
        }

        yield return null;
    }
    #endregion

    #region ThrowCard Functions
    public IEnumerator StartCounter()
    {
        counter = timeToThrow;
        while(counter > 0)
        {
            counter -= Time.deltaTime;
            yield return null;
        }

        if(deckHasTaked)
        {
            ThrowCard(mouseScrollSpeed);
        }

        mouseScrollSpeed = 0;
        canThrow = true;

        yield return null;
    }
    public void ThrowCard(float velocity)
    {
        GameObject newCard;
        newCard = Instantiate(cardPrefab, deckObj.transform.position, deckObj.transform.rotation);

        CardComponent cardComp = newCard.GetComponent<CardComponent>();
        cardComp.CardInfoUpdate(GameManager.Instance.currentDeck[0]);
        
        newCard.GetComponent<Rigidbody>().AddForce(transform.forward * velocity * speedMultiplier);

        cardComp.FlipCardAnim();
    }
    #endregion

    #region SpawnCards Functions
    public IEnumerator SpawnCards(int playerPos, CardInfo cardInfo)
    {
        Brain b = GameManager.Instance.AIInGame[playerPos];
        if(b.hasSplitted)
        {
            if(b.wantFirstCard)
            {
                b.myDecision = Action.Think;
            }
            else
            {
                b.mySecondDecision = Action.Think;
            }

            StartCoroutine(SpawnSplitCards(b, cardInfo));
            GameManager.Instance.SetActiveCardPos(playerPos, b.myDecision, b.mySecondDecision);
            yield break;
        }

        Debug.LogError("CALLED");

        GameObject newCard;
        newCard = Instantiate(cardPrefab, deckObj.transform.position, Quaternion.identity);
        CardComponent cardComp = newCard.GetComponent<CardComponent>();

        StartCoroutine(cardComp.SnapCardPos(firstCardsPos[playerPos]));
        GameManager.Instance.SetActiveCardPos(playerPos, b.myDecision, b.mySecondDecision);
        cardComp.CardInfoUpdate(cardInfo);
        cardComp.FlipCardAnim();

        GameManager.Instance.SetActiveCardPos(playerPos, b.myDecision, b.mySecondDecision);

        yield return null;
    }

    public IEnumerator SpawnDealerCards(CardInfo cardInfo, bool flipCard)
    {
        GameObject newCard;
        newCard = Instantiate(cardPrefab, deckObj.transform.position, Quaternion.identity);
        CardComponent cardComp = newCard.GetComponent<CardComponent>();

        StartCoroutine(cardComp.SnapCardPos(dealerCardPos));
        cardComp.CardInfoUpdate(cardInfo);

        dealerCards.Add(cardComp);
        if(flipCard)
        {
            cardComp.FlipCardAnim();
        }

        yield return null;
    }
    #endregion

    public void ShowDealerCards()
    {
        foreach(CardComponent c in dealerCards)
        {
            c.FlipCardAnim();
        }
    }

    public void SetupSplitBoost(Brain b)
    {
        //modifico posizione prima cardPos
        CardComponent firstCard = cardOnTable[b.tablePos];
        firstCardsPos[b.tablePos].position = firstCard.transform.position;
        //modifico posizione seconda carta
        CardComponent secondCard = cardOnTable[b.tablePos + GameManager.Instance.playerInGame + 1];
        secondCard.transform.position = secondCardsPos[b.tablePos].position;

        //Do due carte alla AI
        StartCoroutine(SpawnSplitCards(b, GameManager.Instance.currentDeck[0]));

        //faccio prendere la nuova decisione alla AI
        StartCoroutine(b.Thinking());
        GameManager.Instance.gamePhase = GamePhase.AITurn;
    }

    public IEnumerator SpawnSplitCards(Brain b, CardInfo cardInfo)
    {
        //spawn first card
        GameObject newCard;
        newCard = Instantiate(cardPrefab, deckObj.transform.position, Quaternion.identity);
        CardComponent cardComp = newCard.GetComponent<CardComponent>();

        if (b.wantFirstCard)
        {
            //snap first card
            StartCoroutine(cardComp.SnapCardPos(firstCardsPos[b.tablePos]));
            cardComp.CardInfoUpdate(cardInfo);
            cardComp.FlipCardAnim();

            GameManager.Instance.SetAINewPoint(b.tablePos, true);

            //disable wantFirstCard bool
            b.wantFirstCard = false;

            //recursive func
            if(b.wantSecondCard)
            {
                StartCoroutine(SpawnSplitCards(b, GameManager.Instance.currentDeck[0]));
            }
        }
        else
        {

            //snap second card
            StartCoroutine(cardComp.SnapCardPos(secondCardsPos[b.tablePos]));
            cardComp.CardInfoUpdate(cardInfo);
            cardComp.FlipCardAnim();

            GameManager.Instance.SetAINewPoint(b.tablePos, false);

            //disable wantSecondCard bool
            b.wantSecondCard = false;
        }

        yield return null;
    }
}
