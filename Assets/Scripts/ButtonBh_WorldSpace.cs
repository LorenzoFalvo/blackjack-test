using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBh_WorldSpace : MonoBehaviour
{
    Camera cam;
    Image image;
    Button button;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        image = GetComponent<Image>();
        button = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject == gameObject)
            {
                image.color = button.colors.highlightedColor;
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("Right Mouse Button has Pressed");

                    image.color = button.colors.pressedColor;
                    button.onClick.Invoke();
                }
            }
            else
            {
                image.color = button.colors.normalColor;
            }
        }
        else
        {
            image.color = Color.white;
        }
    }

}
