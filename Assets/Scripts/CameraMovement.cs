using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    Camera cam;

    float pitch, yaw;
    bool idlePos;

    [Header("Player Clamp Rotation")]
    [Tooltip("max verso il basso")]
    public float minPitch;
    float newMinPitch;
    [Tooltip("max verso l'alto")]
    public float maxPitch;
    float newMaxPitch;
    [Tooltip("max verso destra")]
    public float minYaw;
    float newMinYaw;
    [Tooltip("max verso sinistra")]
    public float maxYaw;
    float newMaxYaw;

    [Header("Mouse Sensitivity")]
    [Tooltip("horizontal sensitivity")]
    public float mouseSensitivityX;

    [Tooltip("vertical sensitivity")]
    public float mouseSensitivityY;

    public Image knobImage;
    Brain lastAIHitted;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.startGame)
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            pitch += mouseY * mouseSensitivityY;
            yaw += mouseX * mouseSensitivityX;

            SetClampValue(minPitch, maxPitch, minYaw, maxYaw);

            pitch = Mathf.Clamp(pitch, newMinPitch, newMaxPitch);
            yaw = Mathf.Clamp(yaw, newMinYaw, newMaxYaw);
            transform.rotation = Quaternion.Euler(-pitch, yaw, 0);

            Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.layer == 8)
                {
                    print("I'm looking at " + hit.transform.name);
                    Brain newAIHitted = hit.transform.GetComponentInParent<Brain>();

                    if (lastAIHitted != null)
                    {
                        if (lastAIHitted != newAIHitted)
                        {
                            lastAIHitted.showInfo = false;
                        }
                    }
                    lastAIHitted = newAIHitted;
                    lastAIHitted.showInfo = true;

                    if (Input.GetKeyDown(KeyCode.Mouse0) && GameManager.Instance.startGame && !GameManager.Instance.pauseGame)
                    {
                        print("Throw Card!");
                        if(lastAIHitted.myDecision == Action.Card || lastAIHitted.mySecondDecision == Action.Card)
                        {
                            GameManager.Instance.DropCardToAI(lastAIHitted.gameObject.GetComponentInParent<Brain>().tablePos);
                            Brain oldAIHitted = lastAIHitted.GetComponent<Brain>();
                            oldAIHitted.showInfo = false;
                        }
                    }
                }
                else
                {
                    if (lastAIHitted != null)
                    {
                        Brain oldAIHitted = lastAIHitted.GetComponentInParent<Brain>();
                        oldAIHitted.showInfo = false;

                        lastAIHitted = null;
                    }
                }
            }
            else
            {
                if (lastAIHitted != null)
                {
                    Brain oldAIHitted = lastAIHitted.GetComponentInParent<Brain>();
                    oldAIHitted.showInfo = false;

                    lastAIHitted = null;
                }
            }

        }
    }

    void SetClampValue(float minY, float maxY, float minX, float maxX)
    {
        newMinPitch = minY;
        newMaxPitch = maxY;
        newMinYaw = minX;
        newMaxYaw = maxX;
    }


}
