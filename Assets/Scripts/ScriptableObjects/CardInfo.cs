using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Timeline;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Card")]
public class CardInfo : ScriptableObject
{

    [SerializeField] string cardName;

    [SerializeField] Material material;

    [SerializeField] int value;
    
    [SerializeField] AudioClip matchCorrectAudio;


    public string CardName => cardName;
    public int CardValue => value;
    public Material Material => material;
    public AudioClip MatchCorrectAudio => matchCorrectAudio;

}
