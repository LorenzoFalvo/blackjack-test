using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Timeline;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CardList")]
public class CardList : ScriptableObject
{
    public List<CardInfo> cardInfo = new List<CardInfo>();
}
