using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardComponent : MonoBehaviour
{
    MeshRenderer myRend;
    Material myBlinkMaterial;

    public CardInfo cardInfo;
    public bool changeInfo;

    Animation anim;
    public LayerMask layerMask;

    public int offsetCardsMultiplier;
    public float speedThrowAnim;
    public int nRound;

    public float lifeSpan;
    public bool canLive;

    void Awake()
    {
        myRend = gameObject.GetComponentInChildren<MeshRenderer>();
        anim = gameObject.GetComponentInChildren<Animation>();
    }

    public void CardInfoUpdate(CardInfo newCard)
    {
        cardInfo = newCard;

        Material[] materials = myRend.materials;
        materials[1] = cardInfo.Material;
        myRend.materials = materials;
    }

    public void FlipCardAnim()
    {
        anim.Play("CardFlip");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != layerMask)
        {
            canLive = true;
            StartCoroutine(SnapCardPos(other.transform));

            int firstTablePos = GameManager.Instance.dealerBh.firstCardsPos.FindIndex(m => m.gameObject == other.gameObject);
            int secondTablePos = GameManager.Instance.dealerBh.secondCardsPos.FindIndex(m => m.gameObject == other.gameObject);
            Brain b;
            if (firstTablePos >= 0)
            {
                b = GameManager.Instance.AIInGame[firstTablePos];
            }
            else
            {
                b = GameManager.Instance.AIInGame[secondTablePos];
            }

            if (b.hasSplitted)
            {
                if (b.wantFirstCard)
                {
                    b.myDecision = Action.Think;
                    b.wantFirstCard = false;
                    GameManager.Instance.SetActiveCardPos(firstTablePos, b.myDecision, b.mySecondDecision);
                    GameManager.Instance.UpdateAIInfo(firstTablePos, true);
                }
                else if (b.wantSecondCard)
                {
                    b.mySecondDecision = Action.Think;
                    b.wantSecondCard = false;
                    GameManager.Instance.SetActiveCardPos(secondTablePos, b.myDecision, b.mySecondDecision);
                    GameManager.Instance.UpdateAIInfo(secondTablePos, false);
                }
            }
            else
            {
                if (firstTablePos >= 0)
                {
                    other.gameObject.SetActive(false);
                    GameManager.Instance.UpdateAIInfo(firstTablePos, true);
                }
                else
                {
                    GameManager.Instance.SetDealerNewPoint();
                }
            }
        }
        else
        {
            Debug.LogError("NOT Correct Position!");
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!canLive)
        {
            StartCoroutine(LifeCoundown());    
        }
    }

    public IEnumerator LifeCoundown()
    {
        while(lifeSpan > 0)
        {
            if(!canLive)
            {
                lifeSpan -= Time.deltaTime;
                yield return null;
            }
            else
            {
                yield return null;
            }
        }
        Destroy(gameObject);
        yield return null;
    }


    public IEnumerator SnapCardPos(Transform firstCardPos)
    {
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<BoxCollider>().enabled = false;

        firstCardPos.Translate(-Vector3.forward * Time.deltaTime * offsetCardsMultiplier);
        firstCardPos.Translate(Vector3.right * Time.deltaTime * offsetCardsMultiplier);
        firstCardPos.position += new Vector3(0f, 0.005f, 0f);

        float speed = speedThrowAnim * Time.deltaTime * Vector3.Distance(transform.position, firstCardPos.position); // calculate distance to move
        while (Vector3.Distance(transform.position, firstCardPos.position) > 0.001f)
        {
            transform.position = Vector3.MoveTowards(transform.position, firstCardPos.position, speed);
            Quaternion lerpRot = Quaternion.Lerp(transform.rotation, firstCardPos.rotation, Vector3.Distance(transform.position, firstCardPos.position));
            transform.rotation = lerpRot;

            yield return null;
        }
        transform.position = firstCardPos.position;
        transform.rotation = firstCardPos.rotation;

        //Add this card into cardOnTable list
        GameManager.Instance.dealerBh.cardOnTable.Add(this);
        yield return null;
    }

}
