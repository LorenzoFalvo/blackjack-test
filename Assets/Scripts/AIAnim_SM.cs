using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAnim_SM : MonoBehaviour
{
    Brain myBrain;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        myBrain = GetComponent<Brain>();
        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (myBrain.myDecision)
        {
            case Action.Card:
                
                SetTriggerAnim("Card");

                break;

            case Action.Stop:

                SetTriggerAnim("Idle");

                break;

            case Action.Lost:

                SetTriggerAnim("Lost");

                break;

            case Action.BlackJack:
                
                break;

            case Action.Won:

                SetTriggerAnim("Won");

                break;

            case Action.Think:

                SetTriggerAnim("Idle");

                break;

            default:
                
                break;
        }
    }

    public void SetTriggerAnim(string name)
    {
        anim.SetTrigger(name);
    }
}
